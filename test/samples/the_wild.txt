[config]
sequence_po_sql = """
    SELECT 
        FFD.FFDDPTN AS LOCATION, 
        TRIM(FFD.FFDARCD) AS STATUS, 
        TRIM(FFD.FFDCUSN) AS CUST, 
        'XL' || NEXT VALUE FOR CDGDEMO.XLSPO_SEQ AS PO_NUMBER 
    FROM PWRDTA.FFDCSTBP AS FFD 
    WHERE FFD.FFDDPTN = '{:0>3}' AND 
    FFD.FFDCUSN = '{: >10}'"""

# mostly common folders
src_directory=C:\ProgramData\xlspo\gc
hst_directory=C:\ProgramData\xlspo\history
wrk_directory=C:\ProgramData\xlspo\working
out_directory=C:\ProgramData\xlspo\output
err_directory=C:\ProgramData\xlspo\errors

file_types=[.xlsx, .xlsm]
column_desc=C
column_item_code=A
column_order_end=P
column_order_start=G
column_upc=B
row_account_number=2
row_location=1
row_order_end=5000
row_order_start=8
row_po_number=3
row_ship_date=5
row_instruction=6
max_qty=100

out_filename=PO_FARMBOY_{count:0>3}_{account}_{timestamp:%Y-%m-%d-%H%M%S}.txt
