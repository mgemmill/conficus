[multiline]
list-of-strings = [
     Wonder Woman
     Superman
     The Flash
     Green Lantern]

list-of-int = [
    12345,
    67890,
    23456,
    78901
    ]

tuple-of-strings = (
     Wonder Woman
     Superman
     The Flash
     Green Lantern)

tuple-of-int = (
    12345,
    67890,
    23456,
    78901
    )

list-of-float = [ 23.56,
    45.78
    34.00
    23.45
    ]


list-of-lists = [
    [1, 2, 3, 4, 5]
    [a, b, c, d, e]
    ]


text = This is a much longer text block
    that we want to preserve but keep
    readable in our configuration file.
    We can used this for email body 
    text, and things like that.


