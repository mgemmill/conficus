debug = true

[integer]
value = 1

[float]
value = 2.0

[empty-list]
value = []

[empty-values]
value1 = 
value2=
value3=      
value4 =        

[single-line-list]
integers = [1, 2, 3, 4]
strings = [one, two, three]
floats = [3.4, 3.4 ]

[empty-tuple]
value = ()

[single-line-tuple]
integers = (1, 2, 3, 4)
strings = (one, two, three)
floats = (3.4, 3.4 )

# [single-line-list-of-tuples]
# list_of_tuples = [(1,2,3,4), (5,6), (7,8,9)]

[bool-true]
val1=true
val2=True
val3=yes
val4=YES
#val5=Y
#val6=t
# added white space
val7=YES    
val8 = yes

[bool-false]
val1 = false
val2 = False
val3 = no
val4 = NO 
#val5 = N 
#val6 = f
# added white space
val7 = NO    

[datetime]
value = 2017-05-31T10:00:00

[date]
value = 2017-05-31


[time]
value = 10:15:02

[path]
windows1 = C:\some\drive
windows2 = C:
windows3 = C:\
unix1 = /some/drive
unix2 = /


[strings]
str1 = unquoted string
str2 = " a quoted string preserves white space "
str3 = "23"
str4 = """Multi-quoted string."""
str5 = """
    This is a single line 
    string.
    """

str6 = """
    This is a multiline\
    string.
    """

str7 = """
    This is a multiline\
    |   string with an\
    |   indent.
    """
