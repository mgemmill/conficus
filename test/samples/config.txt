root_value = true

[root_section]
[root.leaf]
[root.leaf.sub]

# a comment

[with_opt]
name=penguins for stanley
hero = crosby
game: 7
multiline=Henry was a fool for love.
    He made it is dream.
    Fools for one and all.
    What dreams may come!

[inherited]
one = 1
two = 2
three = 3

[inherited.parent]
two = 1
three = 2

[inherited.parent.child]
three = 1
